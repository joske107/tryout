import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { PokemonDto } from '../dto/pokemon.dto';
import { PokemonService } from '../services';

@Controller('pokemons')
export class PokemonController {
    constructor(private readonly pokemonService: PokemonService) { }

    @Get()
    findAll(): PokemonDto[] {
        return this.pokemonService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string): PokemonDto {
        return this.pokemonService.findOne(id);
    }

    @Delete(':id')
    remove(@Param('id') id: string): string {
        return this.pokemonService.remove(id);
    }

    @Post()
    create(@Body() pokemonDto: PokemonDto): string {
        return this.pokemonService.create(pokemonDto);
    }
}

import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as pokemonJson from '../data/pokemons.json';
import { PokemonDto } from '../dto/pokemon.dto';

@Injectable()
export class PokemonService {
    private pokemons: PokemonDto[];
    private newId: number

    constructor() {
        this.pokemons = pokemonJson;
        this.newId = Math.max(...this.pokemons.map(pokemon => pokemon.id)) + 1
    }

    findAll(): PokemonDto[] {
        return this.pokemons;
    }

    findOne(id: string): PokemonDto {
        let result = this.pokemons.find(pokemon => pokemon.id == parseInt(id))
        if (result) return result
        else throw new HttpException('No pokemon with id ' + id, HttpStatus.NOT_FOUND);
    }

    remove(id: string): string {
        let index = this.pokemons.findIndex(pokemon => pokemon.id == parseInt(id))
        if (index == -1) throw new HttpException('No pokemon with id ' + id, HttpStatus.NOT_FOUND);
        this.pokemons.splice(index, 1)
        return "deleted"
    }

    create(pokemonDto: PokemonDto): string {
        pokemonDto.id = this.newId
        this.pokemons.push(pokemonDto)
        return '' + this.newId++
    }
}

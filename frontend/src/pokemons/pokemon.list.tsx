import { Button, Table, Tag } from "antd";
import React, { FC, useEffect, useState } from "react";
import { useHistory } from "react-router";
import { Page } from "../components/page";
import * as pokemonService from "../services/pokemon.service"
export const PokemonList: FC = () => {
  const history = useHistory()
  const [pokemons, setPokemons] = useState<any[]>([])
  useEffect(() => {
    pokemonService.getPokemons().then(value => setPokemons(value))
  }, [])
  const typeColors: { [index: string]: string } = {
    normal: '#A8A878',
    fire: '#F08030',
    fighting: '#C03028',
    water: '#6890F0',
    flying: '#A890F0',
    grass: '#78C850',
    poison: '#A040A0',
    electric: '#F8D030',
    ground: '#E0C068',
    psychic: '#F85888',
    rock: '#B8A038',
    ice: '#98D8D8',
    bug: '#A8B820',
    dragon: '#7038F8',
    ghost: '#705898',
    dark: '#705848',
    steel: '#B8B8D0',
    fairy: '#EE99AC'
  }
  const columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Types',
      dataIndex: 'type',
      key: 'type',
      render: (types: any[]) => (
        <>
          {types.map(type => {
            return (
              <Tag color={typeColors[type.toLowerCase()]} key={type}>
                {type}
              </Tag>
            );
          })}
        </>
      ),
    },
  ]
  return (
    <Page>
      <Button type="primary" onClick={() => history.push("/create")}>New Pokemon</Button>
      <Table dataSource={pokemons} columns={columns} rowKey="id" onRow={(pokemon) => ({
        onClick: () => history.push('/' + pokemon.id)
      })} />
    </Page>
  );
};
import { Button, Tag } from "antd";
import React, { FC, useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { Page } from "../components/page";
import * as pokemonService from "../services/pokemon.service"
import "../styles/pokemonCard.css"

type RouteProps = {
  id: string;
};

export const PokemonDetail: FC = () => {
  const history = useHistory()
  const [pokemon, setPokemon] = useState<any>({name:"Loading..."});
  const { id } = useParams<RouteProps>();
  useEffect(() => {
    pokemonService.getPokemon(id).then(response => {
      if(response.ok){response.json().then(value=>setPokemon(value))}
      else {setPokemon(null)}
    })
  }, [id])
  function deletePokemon(): void {
    pokemonService.deletePokemon(id)
    history.push("/")
  }
  return (
    <Page>
      <Button onClick={() => history.push("/")}>Back</Button>
      {pokemon && <div className="pkcard">
        <div className="pktitle"><div>#{pokemon.id}</div><div>{pokemon.name}</div></div>
        {pokemon.type && <div><b>type</b>
        <div className="pktypes">
          {pokemon.type.map((value: any, index: any) => {
            return <Tag key={index}>{value}</Tag>
          })}
        </div></div>}
        {pokemon.base &&
          <div className="pkbase">
            <b>HP</b>{pokemon.base.HP}
            <b>Attack</b>{pokemon.base.Attack}
            <b>Defense</b>{pokemon.base.Defense}
            <b>Special Attack</b>{pokemon.base["Sp. Attack"]}
            <b>Special Defence</b>{pokemon.base["Sp. Defense"]}
            <b>Speed</b>{pokemon.base.Speed}
          </div>}
        <Button className="pkdelete" type="primary" onClick={() => deletePokemon()} danger>Delete</Button>
      </div>}
      {!pokemon && <h1 className="error">404 This pokemon was not found</h1>}
    </Page>
  );
};

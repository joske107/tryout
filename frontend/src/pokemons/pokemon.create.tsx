import { Button, Form, Input } from "antd"
import React, { FC } from "react"
import { useHistory } from "react-router-dom"
import { Page } from "../components/page"
import * as pokemonService from "../services/pokemon.service"
import "../styles/pokemonCreate.css"

export const PokemonCreate: FC = () => {
    const history = useHistory()
    function submitForm(values: any) {
        pokemonService.createPokemon(values)
        history.push("/")
    }
    return (
        <Page>
        <Button onClick={() => history.push("/")}>Back</Button>
            <h1>Create a new Pokemon!</h1>
            <Form className="createform" onFinish={(values) => submitForm(values)}>
                <Form.Item label="Name" name="name">
                    <Input />
                </Form.Item>
                Types:
                <Form.List name="type">
                    {(fields, { add, remove }) => (
                        <>
                            {fields.map(field => (
                                <Form.Item key={field.key} className="typefield">
                                    <Form.Item {...field} noStyle>
                                        <Input placeholder="type" className="typeinput"/>
                                    </Form.Item>
                                    <Button onClick={() => remove(field.name)} >remove</Button>
                                </Form.Item>
                            ))}
                            <Form.Item>
                                <Button type="dashed" onClick={() => add()}>Add type</Button>
                            </Form.Item>
                        </>
                    )}
                </Form.List>
                <Form.Item label="HP" name={["base", "HP"]}>
                    <Input />
                </Form.Item>
                <Form.Item label="Attack" name={["base", "Attack"]}>
                    <Input />
                </Form.Item>
                <Form.Item label="Defense" name={["base", "Defense"]}>
                    <Input />
                </Form.Item>
                <Form.Item label="Sp. Attack" name={["base", "Sp. Attack"]}>
                    <Input />
                </Form.Item>
                <Form.Item label="Sp. Defense" name={["base", "Sp. Defense"]}>
                    <Input />
                </Form.Item>
                <Form.Item label="Speed" name={["base", "Speed"]}>
                    <Input />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">Submit</Button>
                </Form.Item>
            </Form>
        </Page>
    )
}
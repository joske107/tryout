const URL = 'http://localhost:4000'

export const getPokemons = () => {
    return fetch(URL + "/pokemons")
        .then(response => response.json())
        .catch((error) => {
            console.error("error in getting pokemons: " + error)
        })
}

export const getPokemon = (id: string) => {
    return fetch(URL + "/pokemons/" + id)
}

export const deletePokemon = (id: string) => {
    return fetch(URL + "/pokemons/" + id, { method: 'DELETE' })
        .then(response => response)
        .catch((error) => {
            console.error("error in deleting pokemon with id " + id + ": " + error)
        })
}
export const createPokemon = (pokemon: string) => {
    return fetch(URL + "/pokemons", {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', },
        body: JSON.stringify(pokemon)
    })
        .then(response => response)
        .catch((error) => {
            console.error("error in creating pokemon: " + error)
        })
}